const driversController = require('../controllers').drivers;
const companiesController = require('../controllers').companies;


var models = require('../models');
var express = require('express');
var router = express.Router();

module.exports = (app) => {
    app.get('/api', (req, res) => res.status(200).send({
        message: 'Welcome to the drivers Trucking Link API! Please select a resource',
    }));

    app.post('/api/drivers', driversController.create);

    app.get('/api/drivers', driversController.retrieve);

    app.get('/api/companies/:company_id', companiesController.retrieve);

    app.post('/api/companies', companiesController.create);

    app.put('/api/companies/:company_id', companiesController.update);

    router.post('/api/v1/drivers', function(req, res) {
        var driver = new Driver(req.body);

        driver.save(function(err) {
            if (err) {
                return res.send(err);
            }

            res.send({
                message: 'Movie Added'
            });
        });
    });
};
