'use strict';
module.exports = function(sequelize, DataTypes) {
    var contact = sequelize.define('contact', {
      name: { type: DataTypes.STRING, field: "name" },
      title: { type: DataTypes.STRING, field: "title" },
      email: { type: DataTypes.STRING, field: "email" },
      phone: { type: DataTypes.STRING, field: "phone" },
    }, {
        classMethods: {
            associate: function(models) {
                contact.hasOne(models.address, {
                  foreignKey: "driver"
                });
            }
        }
    });
    return contact;
};
