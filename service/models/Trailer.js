'use strict';
module.exports = function(sequelize, DataTypes) {
    var trailers = sequelize.define('trailers', {
        van: DataTypes.BOOLEAN,
        flatbed: DataTypes.BOOLEAN,
        refrigerated: DataTypes.BOOLEAN,
        tanker: DataTypes.BOOLEAN,
        tanker_endorced: DataTypes.BOOLEAN,
        specialized: DataTypes.BOOLEAN,
        drayage: DataTypes.BOOLEAN,
        other: DataTypes.STRING,
        driver: {
            type: DataTypes.INTEGER,
            allowNull: true,
            field: 'driver',
            references: {
                model: 'drivers',
                key: 'id'
            }
        }
    }, {
        classMethods: {
            //associate: function(models) {
            //    trailers.belongsTo(models.drivers);
            //}
        }
    });
    return trailers;
};
