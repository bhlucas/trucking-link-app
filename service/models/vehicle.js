'use strict';
module.exports = function(sequelize, DataTypes) {
  var vehicle = sequelize.define('vehicle', {
    tractorTrailer: { type: DataTypes.BOOLEAN, field: "tractor_trailer" },
    straightTruck: { type: DataTypes.BOOLEAN, field: "straight_truck" },
    other: DataTypes.STRING,
    driver: {
        type: DataTypes.INTEGER,
        allowNull: true,
        field: 'driver',
        references: {
            model: 'drivers',
            key: 'id'
        }
    }
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return vehicle;
};
