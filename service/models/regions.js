'use strict';
module.exports = function(sequelize, DataTypes) {
  var regions = sequelize.define('regions', {
    northEast: { type: DataTypes.BOOLEAN, field: "north_east" },
    midWest: { type: DataTypes.BOOLEAN, field: "mid_west" },
    southEast: { type: DataTypes.BOOLEAN, field: "south_east" },
    southWest: { type: DataTypes.BOOLEAN, field: "south_west" },
    westCoast: { type: DataTypes.BOOLEAN, field: "west_coast" },
    plains: DataTypes.BOOLEAN,
    driver: {
        type: DataTypes.INTEGER,
        allowNull: true,
        field: 'driver',
        references: {
            model: 'drivers',
            key: 'id'
        }
    }
  }, {
    name:{
      plural: 'regions',
      singular:  'regions'
    },
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      },
      freezeTableName: true
    }
  });
  return regions;
};
