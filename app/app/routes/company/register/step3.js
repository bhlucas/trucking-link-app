import Ember from 'ember';

export default Ember.Route.extend({
  model() {
      return this.modelFor('company.register');
  },
  actions: {
    register() {
      var self = this;
      debugger;
      var company = this.modelFor('company.register');
      company.save().then(function (model) {
        self.transitionTo('company.profile', model.get('id'));
        }, function (error) {
          console.info(error);
        });
    },
    previous() {
      this.transitionTo('company.register.step2');
    }
  }
});
