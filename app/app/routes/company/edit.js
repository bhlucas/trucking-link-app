import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    debugger;
    var model = this.currentModel;
    if(model){
      return model;
    }else{
      debugger;
      var company = this.store.findRecord('company', params.company_id);
      return company;
    }
  },
  actions: {
    update() {
      var self = this;
      debugger;
      var company = this.currentModel;
      company.save().then(function(model) {
        debugger;
        self.transitionTo('company.profile', model.get('id'));
      }, function(error) {
        console.info(error);
      });
    },
    cancel() {
      this.transitionTo('company.profile');
    }
  }
});
