import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    debugger;
    let company = null;
    if (params.company_id) {
       company = this.store.peekRecord('company', params.company_id);
    }
    if(company){
      return company;
    } else {
      return this.modelFor('company');
    }
  },
  actions: {
    register() {
      var self = this;
      debugger;
      var company = this.modelFor('company');
      company.save().then(function (model) {
        self.transitionTo('company.profile', model.get('id'));
        }, function (error) {
          console.info(error.errors[0]);
        });
    },
    previous() {
      this.transitionTo('company.step2');
    }
  }
});
