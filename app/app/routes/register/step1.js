import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.modelFor('register');
  },
  actions: {
    nextStep() {
      debugger;
      this.transitionTo('register.step2');
    }
  }
});
