import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    register(profileType) {
      if(profileType == 'driver'){
        this.transitionToRoute('driver.register.step1');
      }else if(profileType == 'company'){
        this.transitionToRoute('company.register.step1');
      }
    }
  }
});
