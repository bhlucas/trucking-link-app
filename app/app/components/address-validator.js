import Ember from 'ember';

export default Ember.Component.extend({
  // didInsertElement() {
  //   this._super(...arguments);
  //   Ember.$('.ui.form')
  //     .form({
  //       fields: {
  //         name: {
  //           identifier: 'name',
  //           rules: [{
  //             type: 'empty',
  //             prompt: 'Please enter your name'
  //           }]
  //         },
  //       }
  //     });
  // },
  didRender() {
    Ember.$('.ui.form')
      .form({
        on: 'blur',
        onSuccess: function(event) {
          //event.preventDefault()
          // Ember.$('.submit').removeClass('disabled');
          return true;
        },
        onFailure: function(event) {
          //event.preventDefault()
          // Ember.$('.submit').addClass('disabled');
          return false;
        },
        fields: {
          addressOne: {
            identifier: 'addressOne',
            rules: [{
              type: 'empty',
              prompt: 'Please enter your address'
            }]
          },
          city: {
            identifier: 'city',
            rules: [{
              type: 'empty',
              prompt: 'Please enter your city'
            }]
          },
          state: {
            identifier: 'state',
            rules: [{
              type: 'empty',
              prompt: 'Please enter your state'
            }]
          },
          postalCode: {
            identifier: 'postalCode',
            rules: [{
              type: 'empty',
              prompt: 'Please enter your postal code'
            }]
          },
        }
      });
  }
});
