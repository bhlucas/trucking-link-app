import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    selectState(state) {
      this.get('model').set('primaryContact.address.state', state);
    }
  }
});
