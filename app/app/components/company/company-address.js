import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    selectState(state) {
      this.get('model').set('corperation.address.state', state);
    }
  }
});
