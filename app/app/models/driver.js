import DS from 'ember-data';
import { belongsTo } from 'ember-data/relationships';

export default DS.Model.extend({
  dob: DS.attr('string'),
  firstName: DS.attr('string'),
  middleName: DS.attr('string'),
  lastName: DS.attr('string'),
  yearsExperience: DS.attr('number'),
  milesDriven: DS.attr('number'),
  bio: DS.attr('string'),
  twicCard: DS.attr('boolean', { defaultValue: false }),
  trailer: belongsTo('trailer'),
  address: belongsTo('address'),
  regions : belongsTo('regions'),
  vehicle : belongsTo('vehicle'),
  drivingPreference: belongsTo('driving-preference')
});
