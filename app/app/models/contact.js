import DS from 'ember-data';
import { belongsTo } from 'ember-data/relationships';

export default DS.Model.extend({
  name: DS.attr('string'),
  title: DS.attr('string'),
  email: DS.attr('string'),
  phone: DS.attr('string'),
  address: belongsTo('address'),
});
