import DS from 'ember-data';
import { belongsTo } from 'ember-data/relationships';

export default DS.Model.extend({
  phone: DS.attr('string'),
  address: belongsTo('address'),
});
