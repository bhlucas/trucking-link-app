import DS from 'ember-data';
import { belongsTo } from 'ember-data/relationships';

export default DS.Model.extend({
  name: DS.attr('string'),
  carrierName : DS.attr('string'),
  scacCode: DS.attr('string'),
  dotNumber: DS.attr('string'),
  mcNumber: DS.attr('string'),
  terminalLocations: DS.attr('string'),
  regionsServiced: DS.attr('string'),
  website: DS.attr('string'),
  yearsInBusiness: DS.attr('string'),
  satelliteTracking: DS.attr('boolean', { defaultValue: false }),
  electronicLogs: DS.attr('boolean', { defaultValue: false }),
  companyPowerUnits: DS.attr('number'),
  operatorPowerUnits: DS.attr('number'),
  milesPerDay: DS.attr('number'),
  contact: belongsTo('contact'),
  corperation: belongsTo('corperation'),
  trailer: belongsTo('trailer'),
  regions: belongsTo('regions')
});
